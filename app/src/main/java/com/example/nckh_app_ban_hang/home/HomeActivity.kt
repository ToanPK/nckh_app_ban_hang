package com.example.nckh_app_ban_hang.home

import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.example.nckh_app_ban_hang.R
import com.example.nckh_app_ban_hang.base.BaseActivity
import com.example.nckh_app_ban_hang.base.initViewModel
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.menu_home.*

class HomeActivity : BaseActivity<HomeViewModel>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        btn_menu.setOnClickListener {
            draw.openDrawer(nav_home)
        }
    }
    override fun initView() {


    }

    override fun binData() {

    }

    override fun onCreateViewModel(): HomeViewModel = initViewModel()

    override fun layoutId(): Int = R.layout.activity_home
}