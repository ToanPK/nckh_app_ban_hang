package com.example.nckh_app_ban_hang.base

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.nckh_app_ban_hang.home.HomeViewModel
import java.lang.IllegalStateException

class ViewModelFactory(context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        with(modelClass){
            when{
                isAssignableFrom(HomeViewModel::class.java) ->{
                    HomeViewModel()
                }
                else -> throw  IllegalStateException("unknown view model : $modelClass")
            }
        } as T
    companion object {
        fun getInstance(activity: Context): ViewModelFactory = ViewModelFactory(activity)
    }
}