package com.example.nckh_app_ban_hang.base

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

abstract class BaseFragment<VM : BaseViewModel<Any?>> : Fragment() {
    lateinit var internalViewModel : VM
    val viewModel : VM
    get() = internalViewModel


    @SuppressLint("SupportAnnotationUsage")
    @LayoutRes
    abstract fun initView()
    abstract fun binData()
    abstract fun onCreateViewModel() : VM
    abstract fun layoutId() : Int

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        binData()
        internalViewModel = onCreateViewModel()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId(), container, false)
    }

}