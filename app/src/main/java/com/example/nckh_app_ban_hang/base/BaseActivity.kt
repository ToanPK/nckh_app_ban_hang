package com.example.nckh_app_ban_hang.base

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity<VM : BaseViewModel<Any?>>: AppCompatActivity(){
    lateinit var internalViewModel : VM
    val viewModel : VM
    get() = internalViewModel

    abstract fun onCreateViewModel() : VM
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        binData()
        setContentView(layoutId())
        internalViewModel = onCreateViewModel()
    }
    @SuppressLint("SupportAnnotationUsage")
    @LayoutRes
    abstract fun initView()
    abstract fun layoutId() : Int
    abstract fun binData()
}